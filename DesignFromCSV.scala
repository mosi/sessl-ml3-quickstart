/* This is an minimum example of a simulation experiment with SESSL and ML3. You may want to use this a a starting point
for your work. */
object DesignFromCSV extends App {

  import sessl._
  import sessl.ml3._

  execute {
    // The CSVInput trait allows for reading the experiment design from CSV.
    new Experiment with Observation with CSVOutput with CSVInput {
      // Change this if you have a different model.
      model = "sir.ml3"

      // The simulator. Usually it does not make sense to change this.
      simulator = NextReactionMethod()

      // Read experiment design from CSV.
      designFromCSV("design.csv")

      // End time of the simulation.
      stopTime = 200

      // Number of replications.
      replications = 2

      // Initialisation. The model will be initialized with 950 susceptible and 50 infected agents and a random network.
      val s = 950 * "new Person(status := 'susceptible')"
      val i = 50 * "new Person(status := 'infected')"
      initializeWith(s + " " + i) += BarabasiAlbert("Person", "network", 5)

      // Observations will be made every 1 unit of time, between 0 and 200.
      observeAt(range(0, 1, 200))
      // Count agents in each state. See the SESSL User Guide to define other kinds of observations.
      observe("s" ~ agentCount("Person", "ego.status = 'susceptible'"))
      observe("i" ~ agentCount("Person", "ego.status = 'infected'"))
      observe("r" ~ agentCount("Person", "ego.status = 'recovered'"))

      // Writes the results as CSV files in a directory results-*timestamp*.
      withRunResult(writeCSV)
    }
  }
}
