/*
0. INTRODUCTION
This file contains a simple agent-based SIR model of infectious disease transmission in a network. Agents are either
susceptible to the disease, infected, or recovered. Susceptible agent may become infected if they have infected
neighbors. Infected agents recover with a constant rate.
*/

/*
1. AGENT TYPES
Any model should begin with a definition of agent types. An agent type consists of a name, and a comma separated list of
typed attributes. In this case, type is an enumeration type with three possible values (susceptible, infected,
recovered). Other possible types are int, real, string.
*/
Person(status : {"susceptible", "infected", "recovered"})

/*
2. LINKS
Relations between agents are modeled with links. In the following it is defined which links are possible. The following
defines a link "network" between Person-agents. Links are described more thoroughly in [2] (see readme).
*/
network:Person[n]<->[n]Person:network

/*
3. PARAMETERS
Parameters are defined with a name and a type, and an optional default value (e.g.: a : real := 0.03). Usually, values
for parameters are provided in the experiment.
*/
a : real
b : real

/*
4. RULES
Rules define the behavior of agents. Please refer to [1] for an informal description of the concept, and [2] for a
formal definition of the semantics. This model has two rules. The first rule governs infection. Agents that are
currently susceptible may become infected, with a rate depending on the infection parameter "a" and the number of
infected network neighbors. Infected agents recover at a constant rate "b".
*/
Person
| ego.status = "susceptible"
@ a * ego.network.filter(alter.status = "infected").size()
-> ego.status := "infected"

Person
| ego.status = "infected"
@ b
-> ego.status := "recovered"
