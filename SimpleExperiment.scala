package org.sessl

/* This is an minimum example of a simulation experiment with SESSL and ML3. You may want to use this a a starting point
for your work. */
object SimpleExperiment extends App {

  import sessl._
  import sessl.ml3._

  execute {
    // More complex experiments are enabled by mixing in additional traits (with ...). Usually, you want to use at least
    // these three to include observations, parallel execution of replications and outputting the results in csv files.
    new Experiment with Observation with ParallelExecution with CSVOutput {
      // Change this if you have a different model.
      model = "sir.ml3"

      // The simulator. Usually it does not make sense to change this.
      simulator = NextReactionMethod()

      // Setting parameter values.
      set("a" <~ 0.03)
      set("b" <~ 0.05)

      // End time of the simulation.
      stopTime = 200

      // Number of replications.
      replications = 10

      // Initialisation. The model will be initialized with 950 susceptible and 50 infected agents and a random network.
      val s = 950 * "new Person(status := 'susceptible')"
      val i = 50 * "new Person(status := 'infected')"
      initializeWith(s + " " + i) += BarabasiAlbert("Person", "network", 5)

      // Observations will be made every 1 unit of time, between 0 and 200.
      observeAt(range(0, 1, 200))
      // Count agents in each state. See the SESSL User Guide to define other kinds of observations.
      observe("s" ~ agentCount("Person", "ego.status = 'susceptible'"))
      observe("i" ~ agentCount("Person", "ego.status = 'infected'"))
      observe("r" ~ agentCount("Person", "ego.status = 'recovered'"))

      // Writes the results as CSV files in a directory results-*timestamp*.
      withRunResult(writeCSV)
    }
  }
}
