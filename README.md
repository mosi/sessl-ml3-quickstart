# SESSL/ML3 Quick Start #

In this repository you find a number of files to quickly get started with simulation experiments with SESSL and ML3.
You do not need to install any additional software, as all required artifacts are downloaded 
automatically.

The repository contains one example model in ML3, a simple SIR model of infectuous disease (`sir.ml3`), and two example experiments (`SimpleExperiment.scala` and `ModelCheckingExperiment.scala`). 
The supplied experiments can be executed with
* `./mvnw scala:run` or the `run.sh` script on Unix
* `mvnw.cmd scala:run` or the `run.bat` script on Windows
By default, the package is set up to execute `SimpleExperiment.scala` by default. To execute another experiment, you need to change line 53 in `pom.xml`.

To customize the example, you can edit three files:
* Change the SESSL version to use in the pom.xml
* Change the log level (i.e., the verbosity of the console output) in the pom.xml
* Edit the simulation model in the *.ml3 file or create a new one
* Edit the simulation experiment Specification in the .scala file or create a new one

You can rename and replace the *.ml3 and *.scala files. However, make sure that
* the experiment object in the *.scala file is correctly referenced (including the package path) in the pom.xml
* the *.ml3 model file is correctly referenced in the *scala file

# Further Reading about ML3

The Modeling Language for Linked Lives (ML3) is a domain-specific modeling language for agent based models in the social sciences. An informal description of the language can be found in [1]. A formal definition of the language and its semantics can be found in [2]. The models provided with [3] may be of use as further examples. Please not that there have been slight changes to the syntax over the year, so please compare with most recent version in this repository or in [2] if there are any issues.

# Further Reading and Documentation about SESSL

Simulation Experiment Specification via a Scala Layer (SESSL) is a domain-specific language for the specification of simulation experiments [4]. The SESSL-ML3 binding allows for the use of SESSL together with ML3 [3]. For documentation (including a User Guide and a Developer Guide), see [sessl.org](http://sessl.org/).

# References

[1] Tom Warnke, Oliver Reinhardt, Anna Klabunde, Frans Willekens, Adelinde M. Uhrmacher. "Modelling and simulating decision processes of linked lives: An approach based on concurrent processes and stochastic race" In: Population Studies, 71 (sup1) 69-83, 2017.

[2] Oliver Reinhardt, Tom Warnke, Adelinde M. Uhrmacher. "A Language for Agent-Based Discrete-Event Modeling and Simulation of Linked Lives" In: ACM Transactions on Modeling and Computer Simulation (under review).

[3] Oliver Reinhardt, Jason Hilton, Tom Warnke, Jabuk Bijak, Adelinde M. Uhrmacher: "Streamlining Simulation Experiments with Agent-Based Models in Demography" In: Journal of Artificial Societies and Social Simulation, 21 (3) 9, 2018.

[4] Roland Ewald and Adelinde M. Uhrmacher. "SESSL: A Domain-Specific Language for Simulation Experiments". In: ACM Transactions on Modeling and Computer Simulation 24(2), 2014.
